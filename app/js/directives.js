'use strict';
/* Directives */
var mailAppDirectives = angular.module('mailAppDirectives', []);
var mailContainerPosition={"top":0,"left":0,"width":0,"height":0,"parentId":""};

mailAppDirectives.directive('widgetGenerator',function(){
  return{
  restrict:'E',
  templateUrl:'partials/widgetview.html',
  controller:'MailWidgetCtrl'
  };
});

mailAppDirectives.directive('sidePaneGenerator',function(){
  return{
  restrict:'E',
  templateUrl:'partials/side-pane.html'
  };
});

mailAppDirectives.directive('labelViewGenerator',function(){
  return{
  restrict:'E',
  templateUrl:'partials/label-view.html',
  };
});

mailAppDirectives.directive('zoomedMailView',function(){
  return{
  restrict:'E',
  templateUrl:'partials/zoomed-mail.html',
  controller:'MailWidgetCtrl'
  };
});

mailAppDirectives.directive('paneWidgetGenerator',function(){
  return{
  restrict:'E',
  templateUrl:'partials/pane-widget.html'
  };
});

mailAppDirectives.directive('mailGenerator',function($templateCache){
  return{
  restrict:'A',
  templateUrl:'partials/mail-view.html'
  };
});

mailAppDirectives.directive('zoomMailGenerator',function($templateCache){
  return{
  restrict:'A',
  templateUrl:'partials/zoomed-box.html'
  };
});

mailAppDirectives.directive('containerStyleDirective',function(){
  return function(scope, element, attrs) {
     applyStyleOnMailContainer(element);
  };
});

var applyStyleOnMailContainer = function(element){
    var parentObj = element.parent();
    var position = $(parentObj).position(); 
    var width = $(parentObj).width();
    var height = $(parentObj).height();
    if((mailContainerPosition.parentId != $(parentObj).attr("id"))){ 
       mailContainerPosition.parentId = $(parentObj).attr("id");
       mailContainerPosition.width =  width-20; 
       mailContainerPosition.height =  height-20; 
    }else{      
       mailContainerPosition.width = mailContainerPosition.width-20;
       mailContainerPosition.height =  mailContainerPosition.height-20;
    }
    $(element).css("position","absolute");
    $(element).css("height",mailContainerPosition.height+"px");
    $(element).css("width",mailContainerPosition.width+"px");
  };

