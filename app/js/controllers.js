'use strict';

/* Controllers */

var mailAppControllers = angular.module('mailAppControllers', []);
mailAppControllers.run(function($rootScope) {
    $rootScope.enlargedItem = '';
});

mailAppControllers.controller('MailCtrl', ['$rootScope','$location','$routeParams','$scope','Mail',function($rootScope,$location,$routeParams,$scope,Mail){
 	$scope.viewable=true;
  $scope.smallView=false; 
  
  $scope.LoadFile = function() { 
 		$routeParams.filename=LABELSJSONFILE;
  	return Mail.query({filename:$routeParams.filename});
	};

  $scope.zoomIn=function(widgetId){
    $rootScope.enlargedItem=widgetId;
    $location.path('/bigview');
  };
  
  $scope.hideWidgets=function(){
    $scope.viewable=!$scope.viewable;
  };

  $scope.showWidgets=function(){
    $scope.viewable=true;
  };

  $scope.goToLabelView = function() {      
      $location.path("/get-index");
  };
}
]);

mailAppControllers.controller('MailWidgetCtrl', ['$rootScope','$location','$routeParams','$scope','Mail',function($rootScope,$location,$routeParams,$scope,Mail){
  $scope.count=0;
  $scope.remove=false;
  $scope.length=$scope.widgetLength; 

  $scope.dynamicLoadFile = function(fname) {
    $routeParams.filename=fname;
  	$scope.Mails = Mail.query({filename:$routeParams.filename});
  };

  $scope.changeZoom=function(widgetId){
    $routeParams.filename=widgetId;
    $rootScope.enlargedItem=widgetId;
    $location.path('/bigview/'+$routeParams.filename);
  };

  $scope.increment = function(){
    $scope.count =$scope.count+1;
    return $scope.count  
  };

  $scope.showTextForBox = function(count){
      var result= checkMail(count); 
        return (result == undefined) ? false : result.text;
  };

  $scope.showNumberForBox = function(count){
       var result= checkMail(count); 
        return (result == undefined) ? false : result.number;
  };

  $scope.showIdForBox = function(count){
      var result= checkMail(count); 

      return (result == undefined) ? false : result.id;
  };
 
  $scope.getPanes = function(num) {
    return new Array(num);   
  }; 

  var checkMail = function(count){
    if($scope.Mails!=undefined){ 
        return $scope.Mails[count];
    }
  };
}
]);