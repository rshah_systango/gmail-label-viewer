'use strict';

var mailApp= angular.module('mailApp',[ 
'mailAppControllers',
'mailAppServices',
'ngRoute',
'mailAppDirectives'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/bigview', {templateUrl: 'partials/big-view.html',controller:'MailCtrl'});
  $routeProvider.when('/bigview/:widgetId', {templateUrl: 'partials/big-view.html',controller:'MailCtrl'});
  $routeProvider.when('/get-index', {templateUrl: 'partials/label-view.html',controller:'MailCtrl'});
}]);
